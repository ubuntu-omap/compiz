/*
 * Copyright © 2011 Linaro Ltd.
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without
 * fee, provided that the above copyright notice appear in all copies
 * and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the name of
 * Linaro Ltd. not be used in advertising or publicity pertaining to
 * distribution of the software without specific, written prior permission.
 * Linaro Ltd. makes no representations about the suitability of this
 * software for any purpose. It is provided "as is" without express or
 * implied warranty.
 *
 * LINARO LTD. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
 * NO EVENT SHALL LINARO LTD. BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 * WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Authors: Travis Watkins <travis.watkins@linaro.org>
 */

#ifndef _COMPIZ_GLSHADERS_H
#define _COMPIZ_GLSHADERS_H

static const char mainVertexShader[] = "                        \n\
#ifdef GL_ES                                                    \n\
precision mediump float;                                        \n\
#endif                                                          \n\
                                                                \n\
uniform vec3 singleNormal;                                      \n\
uniform mat4 modelview;                                         \n\
uniform mat4 projection;                                        \n\
                                                                \n\
attribute vec3 normal;                                          \n\
attribute vec4 color;                                           \n\
attribute vec2 texCoord0;                                       \n\
attribute vec2 texCoord1;                                       \n\
attribute vec2 texCoord2;                                       \n\
attribute vec2 texCoord3;                                       \n\
attribute vec3 position;                                        \n\
                                                                \n\
@VERTEX_FUNCTIONS@                                              \n\
                                                                \n\
varying vec4 vColor;                                            \n\
varying vec2 vTexCoord0;                                        \n\
varying vec2 vTexCoord1;                                        \n\
varying vec2 vTexCoord2;                                        \n\
varying vec2 vTexCoord3;                                        \n\
                                                                \n\
void main () {                                                  \n\
    vTexCoord0 = texCoord0;                                     \n\
    vTexCoord1 = texCoord1;                                     \n\
    vTexCoord2 = texCoord2;                                     \n\
    vTexCoord3 = texCoord3;                                     \n\
    vColor = color;                                             \n\
                                                                \n\
    gl_Position = projection * modelview * vec4(position, 1.0); \n\
                                                                \n\
    @VERTEX_FUNCTION_CALLS@                                     \n\
                                                                \n\
}";


//params is a 4-part system for determining how to draw the scene
// - x is whether or not to use a single color for drawing
// - y is whether or not to use the varying color for drawing
// - z is the number of texture units to use
// - w is the blend mode to use

//paintAttrib is a list of opacity, brightness, and saturation values
// - x is opacity
// - y is brightness
// - z is saturation

//texture0 is the main texture bound to texture unit 0 from GLWindow::
//glDrawTexture (). texture1, texture2 and texture3 are meant for
//multitexturing and should not be used by plugins.

//TODO: Add multitexturing support.

static const char mainFragmentShader[] = "                                   \n\
#ifdef GL_ES                                                                 \n\
precision mediump float;                                                     \n\
#endif                                                                       \n\
                                                                             \n\
uniform vec4 params;                                                         \n\
uniform vec3 paintAttrib;                                                    \n\
                                                                             \n\
uniform sampler2D texture0;                                                  \n\
uniform sampler2D texture1;                                                  \n\
uniform sampler2D texture2;                                                  \n\
uniform sampler2D texture3;                                                  \n\
                                                                             \n\
uniform vec4 singleColor;                                                    \n\
                                                                             \n\
varying vec4 vColor;                                                         \n\
varying vec2 vTexCoord0;                                                     \n\
varying vec2 vTexCoord1;                                                     \n\
varying vec2 vTexCoord2;                                                     \n\
varying vec2 vTexCoord3;                                                     \n\
                                                                             \n\
@FRAGMENT_FUNCTIONS@                                                         \n\
                                                                             \n\
void main () {                                                               \n\
    vec4 color = vec4 (1, 1, 1, 1);                                          \n\
                                                                             \n\
    if (params.x == 1.0)                                                     \n\
	color *= singleColor;                                                \n\
    if (params.y == 1.0)                                                     \n\
	color *= vColor;                                                     \n\
                                                                             \n\
    if (params.z == 0.25)                                                    \n\
	color *= texture2D (texture0, vTexCoord0);                           \n\
                                                                             \n\
    if (paintAttrib.z != 1.0)                                                \n\
    {                                                                        \n\
	vec3 desaturated = color.rgb * vec3 (0.30, 0.59, 0.11);              \n\
	desaturated = vec3 (dot (desaturated, color.rgb));                   \n\
	color.rgb = color.rgb * vec3 (paintAttrib.z) + desaturated *         \n\
	                                         vec3 (1.0 - paintAttrib.z); \n\
    }                                                                        \n\
                                                                             \n\
    color.rgb = color.rgb * paintAttrib.x * paintAttrib.y;                   \n\
    color.a = color.a * paintAttrib.x;                                       \n\
                                                                             \n\
    gl_FragColor = color;                                                    \n\
                                                                             \n\
    @FRAGMENT_FUNCTION_CALLS@                                                \n\
}";

#endif // _COMPIZ_GLSHADERS_H

