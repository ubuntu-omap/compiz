/*
 * Copyright © 2011 Linaro Ltd.
 * Copyright © 2008 Dennis Kasprzyk
 * Copyright © 2007 Novell, Inc.
 *
 * Permission to use, copy, modify, distribute, and sell this software
 * and its documentation for any purpose is hereby granted without
 * fee, provided that the above copyright notice appear in all copies
 * and that both that copyright notice and this permission notice
 * appear in supporting documentation, and that the name of
 * Dennis Kasprzyk not be used in advertising or publicity pertaining to
 * distribution of the software without specific, written prior permission.
 * Dennis Kasprzyk makes no representations about the suitability of this
 * software for any purpose. It is provided "as is" without express or
 * implied warranty.
 *
 * DENNIS KASPRZYK DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN
 * NO EVENT SHALL DENNIS KASPRZYK BE LIABLE FOR ANY SPECIAL, INDIRECT OR
 * CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
 * OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
 * NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION
 * WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Authors: Dennis Kasprzyk <onestone@compiz-fusion.org>
 *          David Reveman <davidr@novell.com>
 *          Travis Watkins <travis.watkins@linaro.org>
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <errno.h>

#include "privates.h"
#include "shaders.h"

#include <dlfcn.h>
#include <math.h>

namespace GL {
    #ifdef USE_GLES
    EGLCreateImageKHRProc  createImage;
    EGLDestroyImageKHRProc destroyImage;

    GLEGLImageTargetTexture2DOESProc eglImageTargetTexture;

    EGLPostSubBufferNVProc postSubBuffer = NULL;
    #else

    typedef int (*GLXSwapIntervalProc) (int interval);

    GLXSwapIntervalProc      swapInterval = NULL;
    GLXBindTexImageProc      bindTexImage = NULL;
    GLXReleaseTexImageProc   releaseTexImage = NULL;
    GLXQueryDrawableProc     queryDrawable = NULL;
    GLXCopySubBufferProc     copySubBuffer = NULL;
    GLXGetVideoSyncProc      getVideoSync = NULL;
    GLXWaitVideoSyncProc     waitVideoSync = NULL;
    GLXGetFBConfigsProc      getFBConfigs = NULL;
    GLXGetFBConfigAttribProc getFBConfigAttrib = NULL;
    GLXCreatePixmapProc      createPixmap = NULL;
    GLXDestroyPixmapProc     destroyPixmap = NULL;
    GLGenProgramsProc	     genPrograms = NULL;
    GLDeleteProgramsProc     deletePrograms = NULL;
    GLBindProgramProc	     bindProgram = NULL;
    GLProgramStringProc	     programString = NULL;
    GLProgramParameter4fProc programEnvParameter4f = NULL;
    GLProgramParameter4fProc programLocalParameter4f = NULL;
    #endif

    GLActiveTextureProc       activeTexture = NULL;
    GLClientActiveTextureProc clientActiveTexture = NULL;
    GLMultiTexCoord2fProc     multiTexCoord2f = NULL;

    GLGenFramebuffersProc        genFramebuffers = NULL;
    GLDeleteFramebuffersProc     deleteFramebuffers = NULL;
    GLBindFramebufferProc        bindFramebuffer = NULL;
    GLCheckFramebufferStatusProc checkFramebufferStatus = NULL;
    GLFramebufferTexture2DProc   framebufferTexture2D = NULL;
    GLGenerateMipmapProc         generateMipmap = NULL;

    GLBindBufferProc    bindBuffer = NULL;
    GLDeleteBuffersProc deleteBuffers = NULL;
    GLGenBuffersProc    genBuffers = NULL;
    GLBufferDataProc    bufferData = NULL;
    GLBufferSubDataProc bufferSubData = NULL;

    GLGetShaderivProc        getShaderiv = NULL;
    GLGetShaderInfoLogProc   getShaderInfoLog = NULL;
    GLGetProgramivProc       getProgramiv = NULL;
    GLGetProgramInfoLogProc  getProgramInfoLog = NULL;
    GLCreateShaderProc       createShader = NULL;
    GLShaderSourceProc       shaderSource = NULL;
    GLCompileShaderProc      compileShader = NULL;
    GLCreateProgramProc      createProgram = NULL;
    GLAttachShaderProc       attachShader = NULL;
    GLLinkProgramProc        linkProgram = NULL;
    GLValidateProgramProc    validateProgram = NULL;
    GLDeleteShaderProc       deleteShader = NULL;
    GLDeleteProgramProc      deleteProgram = NULL;
    GLUseProgramProc         useProgram = NULL;
    GLGetUniformLocationProc getUniformLocation = NULL;
    GLUniform1fProc          uniform1f = NULL;
    GLUniform1iProc          uniform1i = NULL;
    GLUniform2fProc          uniform2f = NULL;
    GLUniform2iProc          uniform2i = NULL;
    GLUniform3fProc          uniform3f = NULL;
    GLUniform3iProc          uniform3i = NULL;
    GLUniform4fProc          uniform4f = NULL;
    GLUniform4iProc          uniform4i = NULL;
    GLUniformMatrix4fvProc   uniformMatrix4fv = NULL;
    GLGetAttribLocationProc  getAttribLocation = NULL;

    GLEnableVertexAttribArrayProc  enableVertexAttribArray = NULL;
    GLDisableVertexAttribArrayProc disableVertexAttribArray = NULL;
    GLVertexAttribPointerProc      vertexAttribPointer = NULL;

    bool  textureFromPixmap = true;
    bool  textureRectangle = false;
    bool  textureNonPowerOfTwo = false;
    bool  textureNonPowerOfTwoMipmap = false;
    bool  textureEnvCombine = false;
    bool  textureEnvCrossbar = false;
    bool  textureBorderClamp = false;
    bool  textureCompression = false;
    GLint maxTextureSize = 0;
    bool  fbo = false;
    bool  vbo = false;
    bool  shaders = false;
    GLint maxTextureUnits = 1;

    bool canDoSaturated = false;
    bool canDoSlightlySaturated = false;
}

CompOutput *targetOutput = NULL;

GLScreen::GLScreen (CompScreen *s) :
    PluginClassHandler<GLScreen, CompScreen, COMPIZ_OPENGL_ABI> (s),
    priv (new PrivateGLScreen (this))
{
    std::list<GLShaderData *> tempShaders;

    #ifdef USE_GLES
    Display             *xdpy;
    Window               overlay;
    EGLDisplay           dpy;
    EGLConfig            config;
    EGLint               major, minor;
    const char		*eglExtensions, *glExtensions;
    XWindowAttributes    attr;
    EGLint               count, visualid;
    EGLConfig            configs[1024];
    CompOption::Vector   o (0);

    const EGLint config_attribs[] = {
	EGL_SURFACE_TYPE,         EGL_WINDOW_BIT,
	EGL_RED_SIZE,             1,
	EGL_GREEN_SIZE,           1,
	EGL_BLUE_SIZE,            1,
	EGL_ALPHA_SIZE,           0,
	EGL_RENDERABLE_TYPE,      EGL_OPENGL_ES2_BIT,
	EGL_CONFIG_CAVEAT,        EGL_NONE,
	EGL_NONE,
    };

    const EGLint context_attribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
    };

    xdpy = s->dpy ();
    dpy = eglGetDisplay ((EGLNativeDisplayType)xdpy);
    if (!eglInitialize (dpy, &major, &minor))
    {
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    eglBindAPI (EGL_OPENGL_ES_API);

    if (!eglChooseConfig (dpy, config_attribs, configs, 1024, &count))
    {
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    if (!XGetWindowAttributes (xdpy, s->root (), &attr))
    {
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    visualid = XVisualIDFromVisual (attr.visual);
    config = configs[0];
    for (int i = 0; i < count; i++) {
        EGLint val;
        eglGetConfigAttrib (dpy, configs[i], EGL_NATIVE_VISUAL_ID, &val);
        if (visualid == val) {
            config = configs[i];
            break;
        }
    }

    overlay = CompositeScreen::get (s)->overlay ();
    priv->surface = eglCreateWindowSurface (dpy, config, overlay, 0);
    if (priv->surface == EGL_NO_SURFACE)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
	                "eglCreateWindowSurface failed");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    // Currently we rely unconditionally on preserving the buffer contents.
    eglSurfaceAttrib (dpy, priv->surface, EGL_SWAP_BEHAVIOR, EGL_BUFFER_PRESERVED);

    priv->ctx = eglCreateContext (dpy, config, EGL_NO_CONTEXT, context_attribs);
    if (priv->ctx == EGL_NO_CONTEXT)
    {
	compLogMessage ("opengl", CompLogLevelFatal, "eglCreateContext failed");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    if (!eglMakeCurrent (dpy, priv->surface, priv->surface, priv->ctx))
    {
	compLogMessage ("opengl", CompLogLevelFatal,
	                "eglMakeCurrent failed");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    eglExtensions = (const char *) eglQueryString (dpy, EGL_EXTENSIONS);
    glExtensions = (const char *) glGetString (GL_EXTENSIONS);

    if (!glExtensions || !eglExtensions)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"No valid GL extensions string found.");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    GL::textureFromPixmap = true;
    GL::textureNonPowerOfTwo = true;
    GL::fbo = true;
    GL::vbo = true;
    GL::shaders = true;
    GL::maxTextureUnits = 4;
    glGetIntegerv (GL_MAX_TEXTURE_SIZE, &GL::maxTextureSize);

    GL::createImage = (GL::EGLCreateImageKHRProc)
	eglGetProcAddress ("eglCreateImageKHR");
    GL::destroyImage = (GL::EGLDestroyImageKHRProc)
	eglGetProcAddress ("eglDestroyImageKHR");
    GL::eglImageTargetTexture = (GL::GLEGLImageTargetTexture2DOESProc)
	eglGetProcAddress ("glEGLImageTargetTexture2DOES");

    if (!strstr (eglExtensions, "EGL_KHR_image_pixmap") ||
        !strstr (glExtensions, "GL_OES_EGL_image") ||
	!GL::createImage || !GL::destroyImage || !GL::eglImageTargetTexture)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"GL_OES_EGL_image is missing");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

// work around efika supporting GL_BGRA directly instead of via this extension
#ifndef GL_BGRA
    if (!strstr (glExtensions, "GL_EXT_texture_format_BGRA8888"))
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"GL_EXT_texture_format_BGRA8888 is missing");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }
#endif

    if (strstr (glExtensions, "GL_OES_texture_npot"))
	GL::textureNonPowerOfTwoMipmap = true;

    if (strstr (eglExtensions, "EGL_NV_post_sub_buffer"))
	GL::postSubBuffer = (GL::EGLPostSubBufferNVProc)
	    eglGetProcAddress ("eglPostSubBufferNV");

    GL::activeTexture = glActiveTexture;
    GL::genFramebuffers = glGenFramebuffers;
    GL::deleteFramebuffers = glDeleteFramebuffers;
    GL::bindFramebuffer = glBindFramebuffer;
    GL::checkFramebufferStatus = glCheckFramebufferStatus;
    GL::framebufferTexture2D = glFramebufferTexture2D;
    GL::generateMipmap = glGenerateMipmap;

    GL::bindBuffer = glBindBuffer;
    GL::deleteBuffers = glDeleteBuffers;
    GL::genBuffers = glGenBuffers;
    GL::bufferData = glBufferData;
    GL::bufferSubData = glBufferSubData;

    GL::getShaderiv = glGetShaderiv;
    GL::getShaderInfoLog = glGetShaderInfoLog;
    GL::getProgramiv = glGetProgramiv;
    GL::getProgramInfoLog = glGetProgramInfoLog;
    GL::createShader = glCreateShader;
    GL::shaderSource = glShaderSource;
    GL::compileShader = glCompileShader;
    GL::createProgram = glCreateProgram;
    GL::attachShader = glAttachShader;
    GL::linkProgram = glLinkProgram;
    GL::validateProgram = glValidateProgram;
    GL::deleteShader = glDeleteShader;
    GL::deleteProgram = glDeleteProgram;
    GL::useProgram = glUseProgram;
    GL::getUniformLocation = glGetUniformLocation;
    GL::uniform1f = glUniform1f;
    GL::uniform1i = glUniform1i;
    GL::uniform2f = glUniform2f;
    GL::uniform2i = glUniform2i;
    GL::uniform3f = glUniform3f;
    GL::uniform3i = glUniform3i;
    GL::uniform4f = glUniform4f;
    GL::uniform4i = glUniform4i;
    GL::uniformMatrix4fv = glUniformMatrix4fv;
    GL::getAttribLocation = glGetAttribLocation;

    GL::enableVertexAttribArray = glEnableVertexAttribArray;
    GL::disableVertexAttribArray = glDisableVertexAttribArray;
    GL::vertexAttribPointer = glVertexAttribPointer;

    glClearColor (0.0, 0.0, 0.0, 1.0);
    glBlendFunc (GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    glEnable (GL_BLEND);
    glEnable (GL_CULL_FACE);

    priv->updateView ();

    priv->lighting = false;

    priv->filter[NOTHING_TRANS_FILTER] = GLTexture::Fast;
    priv->filter[SCREEN_TRANS_FILTER]  = GLTexture::Good;
    priv->filter[WINDOW_TRANS_FILTER]  = GLTexture::Good;

    if (GL::textureFromPixmap)
	registerBindPixmap (EglTexture::bindPixmapToTexture);

    #else

    Display		 *dpy = s->dpy ();
    XVisualInfo		 templ;
    XVisualInfo		 *visinfo;
    GLXFBConfig		 *fbConfigs;
    int			 defaultDepth, nvisinfo, nElements, value, i;
    const char		 *glxExtensions, *glExtensions;
    GLfloat		 globalAmbient[]  = { 0.1f, 0.1f,  0.1f, 0.1f };
    GLfloat		 ambientLight[]   = { 0.0f, 0.0f,  0.0f, 0.0f };
    GLfloat		 diffuseLight[]   = { 0.9f, 0.9f,  0.9f, 0.9f };
    GLfloat		 light0Position[] = { -0.5f, 0.5f, -9.0f, 1.0f };
    XWindowAttributes    attr;
    XSetWindowAttributes attrib;
    const char           *glRenderer;
    CompOption::Vector o (0);

#if defined (__GLIBC__) && (__GLIBC__ >= 2)
    /* bug #685682 */
    program_invocation_short_name[0] = 'C';
#endif

    if (indirectRendering)
    {
	/* force Mesa libGL into indirect rendering mode, because
	   glXQueryExtensionsString is context-independant */
	setenv ("LIBGL_ALWAYS_INDIRECT", "1", True);
    }

    attrib.override_redirect = 1;

    priv->saveWindow = XCreateWindow (dpy, screen->root (), -100, -100, 1, 1, 0, CopyFromParent, InputOutput, CopyFromParent, CWOverrideRedirect, &attrib);

    if (!XGetWindowAttributes (dpy, s->root (), &attr))
    {
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    templ.visualid = XVisualIDFromVisual (attr.visual);

    visinfo = XGetVisualInfo (dpy, VisualIDMask, &templ, &nvisinfo);
    if (!nvisinfo)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"Couldn't get visual info for default visual");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    defaultDepth = visinfo->depth;

    glXGetConfig (dpy, visinfo, GLX_USE_GL, &value);
    if (!value)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"Root visual is not a GL visual");
	XFree (visinfo);
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    glXGetConfig (dpy, visinfo, GLX_DOUBLEBUFFER, &value);
    if (!value)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"Root visual is not a double buffered GL visual");
	XFree (visinfo);
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    priv->ctx = glXCreateContext (dpy, visinfo, NULL, !indirectRendering);
    if (!priv->ctx)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"glXCreateContext failed");
	XFree (visinfo);

	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    XFree (visinfo);
    glxExtensions = glXQueryExtensionsString (dpy, s->screenNum ());

    if (!strstr (glxExtensions, "GLX_SGIX_fbconfig"))
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"GLX_SGIX_fbconfig is missing");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    priv->getProcAddress = (GL::GLXGetProcAddressProc)
	getProcAddress ("glXGetProcAddressARB");
    GL::bindTexImage = (GL::GLXBindTexImageProc)
	getProcAddress ("glXBindTexImageEXT");
    GL::releaseTexImage = (GL::GLXReleaseTexImageProc)
	getProcAddress ("glXReleaseTexImageEXT");
    GL::queryDrawable = (GL::GLXQueryDrawableProc)
	getProcAddress ("glXQueryDrawable");
    GL::getFBConfigs = (GL::GLXGetFBConfigsProc)
	getProcAddress ("glXGetFBConfigs");
    GL::getFBConfigAttrib = (GL::GLXGetFBConfigAttribProc)
	getProcAddress ("glXGetFBConfigAttrib");
    GL::createPixmap = (GL::GLXCreatePixmapProc)
	getProcAddress ("glXCreatePixmap");
    GL::destroyPixmap = (GL::GLXDestroyPixmapProc)
    	getProcAddress ("glXDestroyPixmap");

    if (!strstr (glxExtensions, "GLX_EXT_texture_from_pixmap") ||
        !GL::bindTexImage || !GL::releaseTexImage)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"GLX_EXT_texture_from_pixmap is missing");
	GL::textureFromPixmap = false;
    }
    else
	GL::textureFromPixmap = true;

    if (!GL::queryDrawable     ||
	!GL::getFBConfigs      ||
	!GL::getFBConfigAttrib ||
	!GL::createPixmap      ||
	!GL::destroyPixmap)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"fbconfig functions missing");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    if (strstr (glxExtensions, "GLX_MESA_copy_sub_buffer"))
	GL::copySubBuffer = (GL::GLXCopySubBufferProc)
	    getProcAddress ("glXCopySubBufferMESA");

    if (strstr (glxExtensions, "GLX_SGI_video_sync"))
    {
	GL::getVideoSync = (GL::GLXGetVideoSyncProc)
	    getProcAddress ("glXGetVideoSyncSGI");

	GL::waitVideoSync = (GL::GLXWaitVideoSyncProc)
	    getProcAddress ("glXWaitVideoSyncSGI");
    }

    if (strstr (glxExtensions, "GLX_SGI_swap_control"))
    {
	GL::swapInterval = (GL::GLXSwapIntervalProc)
	    getProcAddress ("glXSwapIntervalSGI");
    }

    glXMakeCurrent (dpy, CompositeScreen::get (s)->output (), priv->ctx);

    glExtensions = (const char *) glGetString (GL_EXTENSIONS);
    if (!glExtensions)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"No valid GL extensions string found.");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    glRenderer = (const char *) glGetString (GL_RENDERER);
    if (glRenderer != NULL &&
	(strcmp (glRenderer, "Software Rasterizer") == 0 ||
	 strcmp (glRenderer, "Mesa X11") == 0))
    {
	compLogMessage ("opengl",
			CompLogLevelFatal,
			"Software rendering detected");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    if (strstr (glExtensions, "GL_ARB_texture_non_power_of_two")) 
	GL::textureNonPowerOfTwo = true;
    GL::textureNonPowerOfTwoMipmap = GL::textureNonPowerOfTwo;

    glGetIntegerv (GL_MAX_TEXTURE_SIZE, &GL::maxTextureSize);

    if (strstr (glExtensions, "GL_NV_texture_rectangle")  ||
	strstr (glExtensions, "GL_EXT_texture_rectangle") ||
	strstr (glExtensions, "GL_ARB_texture_rectangle"))
    {
	GL::textureRectangle = true;

	if (!GL::textureNonPowerOfTwo)
	{
	    GLint maxTextureSize;

	    glGetIntegerv (GL_MAX_RECTANGLE_TEXTURE_SIZE_NV, &maxTextureSize);
	    if (maxTextureSize > GL::maxTextureSize)
		GL::maxTextureSize = maxTextureSize;
	}
    }

    if (!(GL::textureRectangle || GL::textureNonPowerOfTwo))
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"Support for non power of two textures missing");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    if (strstr (glExtensions, "GL_ARB_texture_env_combine"))
    {
	GL::textureEnvCombine = true;

	/* XXX: GL_NV_texture_env_combine4 need special code but it seams to
	   be working anyway for now... */
	if (strstr (glExtensions, "GL_ARB_texture_env_crossbar") ||
	    strstr (glExtensions, "GL_NV_texture_env_combine4"))
	    GL::textureEnvCrossbar = true;
    }

    if (strstr (glExtensions, "GL_ARB_texture_border_clamp") ||
	strstr (glExtensions, "GL_SGIS_texture_border_clamp"))
	GL::textureBorderClamp = true;

    GL::maxTextureUnits = 1;
    if (strstr (glExtensions, "GL_ARB_multitexture"))
    {
	GL::activeTexture = (GL::GLActiveTextureProc)
	    getProcAddress ("glActiveTexture");
	GL::clientActiveTexture = (GL::GLClientActiveTextureProc)
	    getProcAddress ("glClientActiveTexture");
	GL::multiTexCoord2f = (GL::GLMultiTexCoord2fProc)
	    getProcAddress ("glMultiTexCoord2f");

	if (GL::activeTexture && GL::clientActiveTexture && GL::multiTexCoord2f)
	    glGetIntegerv (GL_MAX_TEXTURE_UNITS_ARB, &GL::maxTextureUnits);
    }

    if (strstr (glExtensions, "GL_EXT_framebuffer_object"))
    {
	GL::genFramebuffers = (GL::GLGenFramebuffersProc)
	    getProcAddress ("glGenFramebuffersEXT");
	GL::deleteFramebuffers = (GL::GLDeleteFramebuffersProc)
	    getProcAddress ("glDeleteFramebuffersEXT");
	GL::bindFramebuffer = (GL::GLBindFramebufferProc)
	    getProcAddress ("glBindFramebufferEXT");
	GL::checkFramebufferStatus = (GL::GLCheckFramebufferStatusProc)
	    getProcAddress ("glCheckFramebufferStatusEXT");
	GL::framebufferTexture2D = (GL::GLFramebufferTexture2DProc)
	    getProcAddress ("glFramebufferTexture2DEXT");
	GL::generateMipmap = (GL::GLGenerateMipmapProc)
	    getProcAddress ("glGenerateMipmapEXT");

	if (GL::genFramebuffers        &&
	    GL::deleteFramebuffers     &&
	    GL::bindFramebuffer        &&
	    GL::checkFramebufferStatus &&
	    GL::framebufferTexture2D   &&
	    GL::generateMipmap)
	    GL::fbo = true;
    }

    if (strstr (glExtensions, "GL_ARB_vertex_buffer_object"))
    {
	GL::bindBuffer = (GL::GLBindBufferProc)
	    getProcAddress ("glBindBufferARB");
	GL::deleteBuffers = (GL::GLDeleteBuffersProc)
	    getProcAddress ("glDeleteBuffersARB");
	GL::genBuffers = (GL::GLGenBuffersProc)
	    getProcAddress ("glGenBuffersARB");
	GL::bufferData = (GL::GLBufferDataProc)
	    getProcAddress ("glBufferDataARB");
	GL::bufferSubData = (GL::GLBufferSubDataProc)
	    getProcAddress ("glBufferSubDataARB");

	if (GL::bindBuffer    &&
	    GL::deleteBuffers &&
	    GL::genBuffers    &&
	    GL::bufferData    &&
	    GL::bufferSubData)
	    GL::vbo = true;
    }

    if (strstr (glExtensions, "GL_ARB_fragment_shader") &&
        strstr (glExtensions, "GL_ARB_vertex_shader"))
    {
	GL::getShaderiv = (GL::GLGetShaderivProc) getProcAddress ("glGetShaderiv");
	GL::getShaderInfoLog = (GL::GLGetShaderInfoLogProc) getProcAddress ("glGetShaderInfoLog");
	GL::getProgramiv = (GL::GLGetProgramivProc) getProcAddress ("glGetProgramiv");
	GL::getProgramInfoLog = (GL::GLGetProgramInfoLogProc) getProcAddress ("glGetProgramInfoLog");
	GL::createShader = (GL::GLCreateShaderProc) getProcAddress ("glCreateShader");
	GL::shaderSource = (GL::GLShaderSourceProc) getProcAddress ("glShaderSource");
	GL::compileShader = (GL::GLCompileShaderProc) getProcAddress ("glCompileShader");
	GL::createProgram = (GL::GLCreateProgramProc) getProcAddress ("glCreateProgram");
	GL::attachShader = (GL::GLAttachShaderProc) getProcAddress ("glAttachShader");
	GL::linkProgram = (GL::GLLinkProgramProc) getProcAddress ("glLinkProgram");
	GL::validateProgram = (GL::GLValidateProgramProc) getProcAddress ("glValidateProgram");
	GL::deleteShader = (GL::GLDeleteShaderProc) getProcAddress ("glDeleteShader");
	GL::deleteProgram = (GL::GLDeleteProgramProc) getProcAddress ("glDeleteProgram");
	GL::useProgram = (GL::GLUseProgramProc) getProcAddress ("glUseProgram");
	GL::getUniformLocation = (GL::GLGetUniformLocationProc) getProcAddress ("glGetUniformLocation");
	GL::uniform1f = (GL::GLUniform1fProc) getProcAddress ("glUniform1f");
	GL::uniform1i = (GL::GLUniform1iProc) getProcAddress ("glUniform1i");
	GL::uniform2f = (GL::GLUniform2fProc) getProcAddress ("glUniform2f");
	GL::uniform2i = (GL::GLUniform2iProc) getProcAddress ("glUniform2i");
	GL::uniform3f = (GL::GLUniform3fProc) getProcAddress ("glUniform3f");
	GL::uniform3i = (GL::GLUniform3iProc) getProcAddress ("glUniform3i");
	GL::uniform4f = (GL::GLUniform4fProc) getProcAddress ("glUniform4f");
	GL::uniform4i = (GL::GLUniform4iProc) getProcAddress ("glUniform4i");
	GL::uniformMatrix4fv = (GL::GLUniformMatrix4fvProc) getProcAddress ("glUniformMatrix4fv");
	GL::getAttribLocation = (GL::GLGetAttribLocationProc) getProcAddress ("glGetAttribLocation");

	GL::enableVertexAttribArray = (GL::GLEnableVertexAttribArrayProc) getProcAddress ("glEnableVertexAttribArray");
	GL::disableVertexAttribArray = (GL::GLDisableVertexAttribArrayProc) getProcAddress ("glDisableVertexAttribArray");
	GL::vertexAttribPointer = (GL::GLVertexAttribPointerProc) getProcAddress ("glVertexAttribPointer");

	GL::shaders = true;
    }

    if (strstr (glExtensions, "GL_ARB_texture_compression"))
	GL::textureCompression = true;

    fbConfigs = (*GL::getFBConfigs) (dpy, s->screenNum (), &nElements);

    for (i = 0; i <= MAX_DEPTH; i++)
    {
	int j, db, stencil, depth, alpha, mipmap, rgba;

	priv->glxPixmapFBConfigs[i].fbConfig       = NULL;
	priv->glxPixmapFBConfigs[i].mipmap         = 0;
	priv->glxPixmapFBConfigs[i].yInverted      = 0;
	priv->glxPixmapFBConfigs[i].textureFormat  = 0;
	priv->glxPixmapFBConfigs[i].textureTargets = 0;

	db      = MAXSHORT;
	stencil = MAXSHORT;
	depth   = MAXSHORT;
	mipmap  = 0;
	rgba    = 0;

	for (j = 0; j < nElements; j++)
	{
	    XVisualInfo *vi;
	    int		visualDepth;

	    vi = glXGetVisualFromFBConfig (dpy, fbConfigs[j]);
	    if (vi == NULL)
		continue;

	    visualDepth = vi->depth;

	    XFree (vi);

	    if (visualDepth != i)
		continue;

	    (*GL::getFBConfigAttrib) (dpy, fbConfigs[j],
				      GLX_ALPHA_SIZE, &alpha);
	    (*GL::getFBConfigAttrib) (dpy, fbConfigs[j],
				      GLX_BUFFER_SIZE, &value);
	    if (value != i && (value - alpha) != i)
		continue;

	    value = 0;
	    if (i == 32)
	    {
		(*GL::getFBConfigAttrib) (dpy, fbConfigs[j],
					  GLX_BIND_TO_TEXTURE_RGBA_EXT, &value);

		if (value)
		{
		    rgba = 1;

		    priv->glxPixmapFBConfigs[i].textureFormat =
			GLX_TEXTURE_FORMAT_RGBA_EXT;
		}
	    }

	    if (!value)
	    {
		if (rgba)
		    continue;

		(*GL::getFBConfigAttrib) (dpy, fbConfigs[j],
					  GLX_BIND_TO_TEXTURE_RGB_EXT, &value);
		if (!value)
		    continue;

		priv->glxPixmapFBConfigs[i].textureFormat =
		    GLX_TEXTURE_FORMAT_RGB_EXT;
	    }

	    (*GL::getFBConfigAttrib) (dpy, fbConfigs[j],
				      GLX_DOUBLEBUFFER, &value);
	    if (value > db)
		continue;

	    db = value;

	    (*GL::getFBConfigAttrib) (dpy, fbConfigs[j],
				      GLX_STENCIL_SIZE, &value);
	    if (value > stencil)
		continue;

	    stencil = value;

	    (*GL::getFBConfigAttrib) (dpy, fbConfigs[j],
				      GLX_DEPTH_SIZE, &value);
	    if (value > depth)
		continue;

	    depth = value;

	    if (GL::fbo)
	    {
		(*GL::getFBConfigAttrib) (dpy, fbConfigs[j],
					  GLX_BIND_TO_MIPMAP_TEXTURE_EXT,
					  &value);
		if (value < mipmap)
		    continue;

		mipmap = value;
	    }

	    (*GL::getFBConfigAttrib) (dpy, fbConfigs[j],
				      GLX_Y_INVERTED_EXT, &value);

	    priv->glxPixmapFBConfigs[i].yInverted = value;

	    (*GL::getFBConfigAttrib) (dpy, fbConfigs[j],
				      GLX_BIND_TO_TEXTURE_TARGETS_EXT, &value);

	    priv->glxPixmapFBConfigs[i].textureTargets = value;

	    priv->glxPixmapFBConfigs[i].fbConfig = fbConfigs[j];
	    priv->glxPixmapFBConfigs[i].mipmap   = mipmap;
	}
    }

    if (nElements)
	XFree (fbConfigs);

    if (!priv->glxPixmapFBConfigs[defaultDepth].fbConfig)
    {
	compLogMessage ("opengl", CompLogLevelFatal,
			"No GLXFBConfig for default depth, "
			"this isn't going to work.");
	screen->handleCompizEvent ("opengl", "fatal_fallback", o);
	setFailed ();
	return;
    }

    glClearColor (0.0, 0.0, 0.0, 1.0);
    glBlendFunc (GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    glEnable (GL_BLEND);
    glEnable (GL_CULL_FACE);
    glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glColor4usv (defaultColor);
    glEnableClientState (GL_VERTEX_ARRAY);
    glEnableClientState (GL_TEXTURE_COORD_ARRAY);

    if (GL::textureEnvCombine && GL::maxTextureUnits >= 2)
    {
	GL::canDoSaturated = true;
	if (GL::textureEnvCrossbar && GL::maxTextureUnits >= 4)
	    GL::canDoSlightlySaturated = true;
    }

    priv->updateView ();

    glLightModelfv (GL_LIGHT_MODEL_AMBIENT, globalAmbient);

    glEnable (GL_LIGHT0);
    glLightfv (GL_LIGHT0, GL_AMBIENT, ambientLight);
    glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuseLight);
    glLightfv (GL_LIGHT0, GL_POSITION, light0Position);

    glColorMaterial (GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

    glNormal3f (0.0f, 0.0f, -1.0f);

    priv->lighting = false;

    priv->filter[NOTHING_TRANS_FILTER] = GLTexture::Fast;
    priv->filter[SCREEN_TRANS_FILTER]  = GLTexture::Good;
    priv->filter[WINDOW_TRANS_FILTER]  = GLTexture::Good;

    if (GL::textureFromPixmap)
	registerBindPixmap (TfpTexture::bindPixmapToTexture);
    #endif

#if defined (__GLIBC__) && (__GLIBC__ >= 2)
    /* bug #685682 */
    program_invocation_short_name[0] = 'c';
#endif

    priv->scratchFbo = new GLFramebufferObject;
    priv->scratchFbo->allocate (*screen, NULL, GL_BGRA);

    tempShaders.push_back(&PrivateGLScreen::mainShaders);
    GLVertexBuffer::streamingBuffer ()->setProgram (getProgram (tempShaders));
}

GLScreen::~GLScreen ()
{
    if (priv->hasCompositing)
	CompositeScreen::get (screen)->unregisterPaintHandler ();

    #ifdef USE_GLES
    Display *xdpy = screen->dpy ();
    EGLDisplay dpy = eglGetDisplay (xdpy);

    eglMakeCurrent (dpy, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    eglDestroyContext (dpy, priv->ctx);
    eglDestroySurface (dpy, priv->surface);
    eglTerminate (dpy);
    eglReleaseThread ();
    #else

    glXDestroyContext (screen->dpy (), priv->ctx);
    #endif

    delete priv->scratchFbo;
    delete priv;
}

PrivateGLScreen::PrivateGLScreen (GLScreen   *gs) :
    gScreen (gs),
    cScreen (CompositeScreen::get (screen)),
    textureFilter (GL_LINEAR),
    backgroundTextures (),
    backgroundLoaded (false),
    rasterPos (0, 0),
    projection (NULL),
    clearBuffers (true),
    lighting (false),
    #ifndef USE_GLES
    getProcAddress (0),
    #endif
    scratchFbo (NULL),
    outputRegion (),
    pendingCommands (false),
    lastMask (0),
    bindPixmap (),
    hasCompositing (false),
    programCache (new GLProgramCache (30))
{
    ScreenInterface::setHandler (screen);
}

GLShaderData PrivateGLScreen::mainShaders = {
    "opengl", mainVertexShader, mainFragmentShader };

PrivateGLScreen::~PrivateGLScreen ()
{
    delete programCache;
}

GLushort defaultColor[4] = { 0xffff, 0xffff, 0xffff, 0xffff };



GLenum
GLScreen::textureFilter ()
{
    return priv->textureFilter;
}

void
GLScreen::setTextureFilter (GLenum filter)
{
    priv->textureFilter = filter;
}

void
PrivateGLScreen::handleEvent (XEvent *event)
{
    CompWindow *w;

    screen->handleEvent (event);

    switch (event->type) {
	case PropertyNotify:
	    if (event->xproperty.atom == Atoms::xBackground[0] ||
		event->xproperty.atom == Atoms::xBackground[1])
	    {
		if (event->xproperty.window == screen->root ())
		    gScreen->updateBackground ();
	    }
	    else if (event->xproperty.atom == Atoms::winOpacity ||
		     event->xproperty.atom == Atoms::winBrightness ||
		     event->xproperty.atom == Atoms::winSaturation)
	    {
		w = screen->findWindow (event->xproperty.window);
		if (w)
		    GLWindow::get (w)->updatePaintAttribs ();
	    }
	    else if (event->xproperty.atom == Atoms::wmIcon)
	    {
		w = screen->findWindow (event->xproperty.window);
		if (w)
		    GLWindow::get (w)->priv->icons.clear ();
	    }
	    break;
	break;
	default:
	    if (event->type == cScreen->damageEvent () + XDamageNotify)
	    {
		XDamageNotifyEvent *de = (XDamageNotifyEvent *) event;

		#ifdef USE_GLES
		std::map<Damage, EglTexture*>::iterator it =
		    boundPixmapTex.find (de->damage);
		#else
		std::map<Damage, TfpTexture*>::iterator it =
		    boundPixmapTex.find (de->damage);
		#endif
		if (it != boundPixmapTex.end ())
		{
		    it->second->damaged = true;
		}
	    }
	    break;
    }
}

void
GLScreen::clearTargetOutput (unsigned int mask)
{
    clearOutput (targetOutput, mask);
}


static void
frustum (GLfloat *m,
	 GLfloat left,
	 GLfloat right,
	 GLfloat bottom,
	 GLfloat top,
	 GLfloat nearval,
	 GLfloat farval)
{
    GLfloat x, y, a, b, c, d;

    x = (2.0 * nearval) / (right - left);
    y = (2.0 * nearval) / (top - bottom);
    a = (right + left) / (right - left);
    b = (top + bottom) / (top - bottom);
    c = -(farval + nearval) / ( farval - nearval);
    d = -(2.0 * farval * nearval) / (farval - nearval);

#define M(row,col)  m[col * 4 + row]
    M(0,0) = x;     M(0,1) = 0.0f;  M(0,2) = a;      M(0,3) = 0.0f;
    M(1,0) = 0.0f;  M(1,1) = y;     M(1,2) = b;      M(1,3) = 0.0f;
    M(2,0) = 0.0f;  M(2,1) = 0.0f;  M(2,2) = c;      M(2,3) = d;
    M(3,0) = 0.0f;  M(3,1) = 0.0f;  M(3,2) = -1.0f;  M(3,3) = 0.0f;
#undef M

}

static void
perspective (GLfloat *m,
	     GLfloat fovy,
	     GLfloat aspect,
	     GLfloat zNear,
	     GLfloat zFar)
{
    GLfloat xmin, xmax, ymin, ymax;

    ymax = zNear * tan (fovy * M_PI / 360.0);
    ymin = -ymax;
    xmin = ymin * aspect;
    xmax = ymax * aspect;

    frustum (m, xmin, xmax, ymin, ymax, zNear, zFar);
}

void
PrivateGLScreen::updateView ()
{
    GLfloat projection_array[16];

    #ifndef USE_GLES
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
    glDepthRange (0, 1);
    glRasterPos2f (0, 0);
    #endif
    glViewport (-1, -1, 2, 2);

    rasterPos = CompPoint (0, 0);

    perspective (projection_array, 60.0f, 1.0f, 0.1f, 100.0f);

    if (projection != NULL)
	delete projection;
    projection = new GLMatrix (projection_array);

    #ifndef USE_GLES
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glMultMatrixf (projection_array);
    glMatrixMode (GL_MODELVIEW);
    #endif

    CompRegion region (screen->region ());
    /* remove all output regions from visible screen region */
    foreach (CompOutput &o, screen->outputDevs ())
	region -= o;

    /* we should clear color buffers before swapping if we have visible
	regions without output */
    clearBuffers = !region.isEmpty ();

    gScreen->setDefaultViewport ();
}

void
PrivateGLScreen::outputChangeNotify ()
{
    screen->outputChangeNotify ();

    scratchFbo->allocate (*screen, NULL, GL_BGRA);
    updateView ();
}

#ifndef USE_GLES
GL::FuncPtr
GLScreen::getProcAddress (const char *name)
{
    static void *dlhand = NULL;
    GL::FuncPtr funcPtr = NULL;

    if (priv->getProcAddress)
	funcPtr = priv->getProcAddress ((GLubyte *) name);

    if (!funcPtr)
    {
	if (!dlhand)
	    dlhand = dlopen ("libopengl.so", RTLD_LAZY);

	if (dlhand)
	{
	    dlerror ();
	    funcPtr = (GL::FuncPtr) dlsym (dlhand, name);
	    if (dlerror () != NULL)
		funcPtr = NULL;
	}
    }

    return funcPtr;
}
#endif

void
PrivateGLScreen::updateScreenBackground ()
{
    Display	  *dpy = screen->dpy ();
    Atom	  pixmapAtom, actualType;
    int		  actualFormat, i, status;
    unsigned int  width = 1, height = 1, depth = 0;
    unsigned long nItems;
    unsigned long bytesAfter;
    unsigned char *prop;
    Pixmap	  pixmap = None;

    pixmapAtom = XInternAtom (dpy, "PIXMAP", false);

    for (i = 0; pixmap == 0 && i < 2; i++)
    {
	status = XGetWindowProperty (dpy, screen->root (),
				     Atoms::xBackground[i],
				     0, 4, false, AnyPropertyType,
				     &actualType, &actualFormat, &nItems,
				     &bytesAfter, &prop);

	if (status == Success && nItems && prop)
	{
	    if (actualType   == pixmapAtom &&
		actualFormat == 32         &&
		nItems	     == 1)
	    {
		Pixmap p = None;

		memcpy (&p, prop, 4);

		if (p)
		{
		    unsigned int ui;
		    int		 i;
		    Window	 w;

		    if (XGetGeometry (dpy, p, &w, &i, &i,
				      &width, &height, &ui, &depth))
		    {
			if ((int) depth == screen->attrib ().depth)
			    pixmap = p;
		    }
		}
	    }

	    XFree (prop);
	}
    }

    if (pixmap)
    {
	backgroundTextures =
	    GLTexture::bindPixmapToTexture (pixmap, width, height, depth);
	if (backgroundTextures.empty ())
	{
	    compLogMessage ("core", CompLogLevelWarn,
			    "Couldn't bind background pixmap 0x%x to "
			    "texture", (int) pixmap);
	}
    }
    else
    {
	backgroundTextures.clear ();
    }

    if (backgroundTextures.empty () && backgroundImage)
    {
	CompSize   size;
	CompString fileName (backgroundImage);
	CompString pname ("");

	backgroundTextures = GLTexture::readImageToTexture (fileName, pname, size);
    }
}

void
GLScreen::setTexEnvMode (GLenum mode)
{
    #ifndef USE_GLES
    if (priv->lighting)
	glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    else
	glTexEnvi (GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, mode);
    #endif
}

void
GLScreen::setLighting (bool lighting)
{
    #ifndef USE_GLES
    if (priv->lighting != lighting)
    {
	if (!priv->optionGetLighting ())
	    lighting = false;

	if (lighting)
	{
	    glEnable (GL_COLOR_MATERIAL);
	    glEnable (GL_LIGHTING);
	}
	else
	{
	    glDisable (GL_COLOR_MATERIAL);
	    glDisable (GL_LIGHTING);
	}

	priv->lighting = lighting;

	setTexEnvMode (GL_REPLACE);
    }
    #endif
}

bool
GLScreenInterface::glPaintOutput (const GLScreenPaintAttrib &sAttrib,
			          const GLMatrix            &transform,
			          const CompRegion          &region,
			          CompOutput                *output,
			          unsigned int              mask)
    WRAPABLE_DEF (glPaintOutput, sAttrib, transform, region, output, mask)

void
GLScreenInterface::glPaintTransformedOutput (const GLScreenPaintAttrib &sAttrib,
					     const GLMatrix            &transform,
					     const CompRegion          &region,
					     CompOutput                *output,
					     unsigned int              mask)
    WRAPABLE_DEF (glPaintTransformedOutput, sAttrib, transform, region,
		  output, mask)

void
GLScreenInterface::glApplyTransform (const GLScreenPaintAttrib &sAttrib,
				     CompOutput                *output,
				     GLMatrix                  *transform)
    WRAPABLE_DEF (glApplyTransform, sAttrib, output, transform)

void
GLScreenInterface::glEnableOutputClipping (const GLMatrix   &transform,
				           const CompRegion &region,
				           CompOutput       *output)
    WRAPABLE_DEF (glEnableOutputClipping, transform, region, output)

void
GLScreenInterface::glDisableOutputClipping ()
    WRAPABLE_DEF (glDisableOutputClipping)

GLMatrix *
GLScreenInterface::projectionMatrix ()
    WRAPABLE_DEF (projectionMatrix)

void
GLScreenInterface::glPaintCompositedOutput (const CompRegion    &region,
					    GLFramebufferObject *fbo,
					    unsigned int         mask)
    WRAPABLE_DEF (glPaintCompositedOutput, region, fbo, mask)


GLMatrix *
GLScreen::projectionMatrix ()
{
    WRAPABLE_HND_FUNC_RETURN (5, GLMatrix *, projectionMatrix)

    return priv->projection;
}

void
GLScreen::updateBackground ()
{
    priv->backgroundTextures.clear ();

    if (priv->backgroundLoaded)
    {
	priv->backgroundLoaded = false;
	CompositeScreen::get (screen)->damageScreen ();
    }
}

bool
GLScreen::lighting ()
{
    return priv->lighting;
}

GLTexture::Filter
GLScreen::filter (int filter)
{
    return priv->filter[filter];
}

void
GLScreen::setFilter (int num, GLTexture::Filter filter)
{
    priv->filter[num] = filter;
}

#ifndef USE_GLES
GLFBConfig*
GLScreen::glxPixmapFBConfig (unsigned int depth)
{
    return &priv->glxPixmapFBConfigs[depth];
}
#endif

void
GLScreen::clearOutput (CompOutput   *output,
		       unsigned int mask)
{
    BoxPtr pBox = &output->region ()->extents;

    if (pBox->x1 != 0	     ||
	pBox->y1 != 0	     ||
	pBox->x2 != (int) screen->width () ||
	pBox->y2 != (int) screen->height ())
    {
	glEnable (GL_SCISSOR_TEST);
	glScissor (pBox->x1,
		   screen->height () - pBox->y2,
		   pBox->x2 - pBox->x1,
		   pBox->y2 - pBox->y1);
	glClear (mask);
	glDisable (GL_SCISSOR_TEST);
    }
    else
    {
	glClear (mask);
    }
}

void
GLScreen::setDefaultViewport ()
{
    priv->lastViewport.x      = screen->outputDevs ()[0].x1 ();
    priv->lastViewport.y      = screen->height () -
				screen->outputDevs ()[0].y2 ();
    priv->lastViewport.width  = screen->outputDevs ()[0].width ();
    priv->lastViewport.height = screen->outputDevs ()[0].height ();

    glViewport (priv->lastViewport.x,
		priv->lastViewport.y,
		priv->lastViewport.width,
		priv->lastViewport.height);
}

#ifdef USE_GLES
EGLContext
GLScreen::getEGLContext ()
{
    return priv->ctx;
}
#endif

GLProgram *
GLScreen::getProgram (std::list<GLShaderData*> shaders)
{
    return (*priv->programCache)(shaders);
}

namespace GL
{

void
waitForVideoSync ()
{
#ifndef USE_GLES
    if (GL::waitVideoSync)
    {
	// Don't wait twice. Just in case.
	if (GL::swapInterval)
	    (*GL::swapInterval) (0);

	// Docs: http://www.opengl.org/registry/specs/SGI/video_sync.txt
	unsigned int frameno;
	(*GL::waitVideoSync) (1, 0, &frameno);
    }
#endif
}

void
controlSwapVideoSync (bool sync)
{
#ifndef USE_GLES
    // Docs: http://www.opengl.org/registry/specs/SGI/swap_control.txt
    if (GL::swapInterval)
	(*GL::swapInterval) (sync ? 1 : 0);
    else if (sync)
	waitForVideoSync ();
#endif
}

} // namespace GL

void
PrivateGLScreen::paintOutputs (CompOutput::ptrList &outputs,
			       unsigned int        mask,
			       const CompRegion    &region)
{
    XRectangle r;

    GLFramebufferObject *oldFbo = NULL;
    bool useFbo = false;

    oldFbo = scratchFbo->bind ();
    useFbo = scratchFbo->checkStatus () && scratchFbo->tex ();
    if (!useFbo) {
printf ("bailing!");
	GLFramebufferObject::rebind (oldFbo);
    }

    refreshSubBuffer = ((lastMask & COMPOSITE_SCREEN_DAMAGE_ALL_MASK) &&
                        !(mask & COMPOSITE_SCREEN_DAMAGE_ALL_MASK) &&
                        (mask & COMPOSITE_SCREEN_DAMAGE_REGION_MASK));

    if (refreshSubBuffer)
    {
	// FIXME: We shouldn't have to substract a 1X1 pixel region here !!
	// This is an ugly workaround for what appears to be a bug in the SGX
	// X11 driver (e.g. on Pandaboard OMAP4 platform).
	// Posting a fullscreen damage region to the SGX seems to reset the
	// framebuffer, causing the screen to blackout.
	cScreen->damageRegion (CompRegion (screen->fullscreenOutput ()) -
                               CompRegion (CompRect(0, 0, 1, 1)));
    }

    if (clearBuffers)
    {
	if (mask & COMPOSITE_SCREEN_DAMAGE_ALL_MASK)
	    glClear (GL_COLOR_BUFFER_BIT);
    }

    CompRegion tmpRegion (region);

    foreach (CompOutput *output, outputs)
    {
	targetOutput = output;

	r.x	 = output->x1 ();
	r.y	 = screen->height () - output->y2 ();
	r.width  = output->width ();
	r.height = output->height ();

	if (lastViewport.x      != r.x     ||
	    lastViewport.y      != r.y     ||
	    lastViewport.width  != r.width ||
	    lastViewport.height != r.height)
	{
	    glViewport (r.x, r.y, r.width, r.height);
	    lastViewport = r;
	}

#ifdef USE_GLES
	if (mask & COMPOSITE_SCREEN_DAMAGE_ALL_MASK || !GL::postSubBuffer)
#else
	if (mask & COMPOSITE_SCREEN_DAMAGE_ALL_MASK)
#endif
	{
	    GLMatrix identity;

	    gScreen->glPaintOutput (defaultScreenPaintAttrib,
				    identity,
				    CompRegion (*output), output,
				    PAINT_SCREEN_REGION_MASK |
				    PAINT_SCREEN_FULL_MASK);
	}
	else if (mask & COMPOSITE_SCREEN_DAMAGE_REGION_MASK)
	{
	    GLMatrix identity;

	    if (refreshSubBuffer)
		tmpRegion = CompRegion (*output);

	    outputRegion = tmpRegion & CompRegion (*output);

	    if (!gScreen->glPaintOutput (defaultScreenPaintAttrib,
					 identity,
					 outputRegion, output,
					 PAINT_SCREEN_REGION_MASK))
	    {
		identity.reset ();

		gScreen->glPaintOutput (defaultScreenPaintAttrib,
					identity,
					CompRegion (*output), output,
					PAINT_SCREEN_FULL_MASK);

		tmpRegion += *output;
	    }
	}
    }

    targetOutput = &screen->outputDevs ()[0];
    glFlush ();

    if (useFbo)
    {
	GLFramebufferObject::rebind (oldFbo);

	// FIXME: does not work if screen dimensions exceed max texture size
	gScreen->glPaintCompositedOutput (tmpRegion, scratchFbo, mask);
    }

    // FIXME: Actually fix the composite plugin to be more efficient;
    // If GL::swapInterval == NULL && GL::waitVideoSync != NULL then the
    // composite plugin should not be imposing any framerate restriction
    // (ie. blocking the CPU) at all. Because the framerate will be controlled
    // and optimized here:
#ifdef USE_GLES
    if (mask & COMPOSITE_SCREEN_DAMAGE_ALL_MASK || !GL::postSubBuffer)
#else
    if (mask & COMPOSITE_SCREEN_DAMAGE_ALL_MASK)
#endif
     {
	#ifdef USE_GLES
	Display *xdpy = screen->dpy ();
	eglSwapBuffers (eglGetDisplay (xdpy), surface);
	eglWaitGL ();
	XFlush (xdpy);
	#else
	// controlSwapVideoSync is much faster than waitForVideoSync because
	// it won't block the CPU. The waiting is offloaded to the GPU.
	// Unfortunately it only works with glXSwapBuffers in most drivers.

	GL::controlSwapVideoSync (optionGetSyncToVblank ());
	glXSwapBuffers (screen->dpy (), cScreen->output ());
	#endif
    }
    else
    {
	BoxPtr pBox;
	int    nBox, y;

	GL::waitForVideoSync ();
	pBox = const_cast <Region> (tmpRegion.handle ())->rects;
	nBox = const_cast <Region> (tmpRegion.handle ())->numRects;

	#ifdef USE_GLES
	Display *xdpy = screen->dpy ();

	if (GL::postSubBuffer)
	{
	    while (nBox--)
	    {
		y = screen->height () - pBox->y2;

		(*GL::postSubBuffer) (eglGetDisplay (xdpy), surface,
				      pBox->x1, y,
				      pBox->x2 - pBox->x1,
				      pBox->y2 - pBox->y1);
		pBox++;
	    }
	}
	else
	{
	    eglSwapBuffers (eglGetDisplay (xdpy), surface);
	}

	eglWaitGL ();
	XFlush (xdpy);

	#else
	if (GL::copySubBuffer)
	{
	    while (nBox--)
	    {
		y = screen->height () - pBox->y2;

		(*GL::copySubBuffer) (screen->dpy (), cScreen->output (),
				      pBox->x1, y,
				      pBox->x2 - pBox->x1,
				      pBox->y2 - pBox->y1);

		pBox++;
	    }
	}
	else
	{
	    glEnable (GL_SCISSOR_TEST);
	    glDrawBuffer (GL_FRONT);

	    while (nBox--)
	    {
		y = screen->height () - pBox->y2;

		glBitmap (0, 0, 0, 0,
			  pBox->x1 - rasterPos.x (),
			  y - rasterPos.y (),
			  NULL);

		rasterPos = CompPoint (pBox->x1, y);

		glScissor (pBox->x1, y,
			   pBox->x2 - pBox->x1,
			   pBox->y2 - pBox->y1);

		glCopyPixels (pBox->x1, y,
			      pBox->x2 - pBox->x1,
			      pBox->y2 - pBox->y1,
			      GL_COLOR);

		pBox++;
	    }

	    glDrawBuffer (GL_BACK);
	    glDisable (GL_SCISSOR_TEST);
	    glFlush ();
	}
	#endif
    }

    lastMask = mask;
}

bool
PrivateGLScreen::hasVSync ()
{
   #ifdef USE_GLES
   return false;
   #else
   return (GL::getVideoSync && optionGetSyncToVblank ());
   #endif
}

void
PrivateGLScreen::prepareDrawing ()
{
    if (pendingCommands)
    {
	// glFlush! glFinish would block the CPU, which is bad.
	glFlush ();
	pendingCommands = false;
    }
}

GLTexture::BindPixmapHandle
GLScreen::registerBindPixmap (GLTexture::BindPixmapProc proc)
{
    priv->bindPixmap.push_back (proc);
    if (!priv->hasCompositing &&
	CompositeScreen::get (screen)->registerPaintHandler (priv))
	priv->hasCompositing = true;
    return priv->bindPixmap.size () - 1;
}

void
GLScreen::unregisterBindPixmap (GLTexture::BindPixmapHandle hnd)
{
    bool hasBP = false;
    priv->bindPixmap[hnd].clear ();
    for (unsigned int i = 0; i < priv->bindPixmap.size (); i++)
	if (!priv->bindPixmap[i].empty ())
	    hasBP = true;
    if (!hasBP && priv->hasCompositing)
    {
	CompositeScreen::get (screen)->unregisterPaintHandler ();
	priv->hasCompositing = false;
    }
}

GLFramebufferObject *
GLScreen::fbo ()
{
    return priv->scratchFbo;
}

GLTexture *
GLScreen::defaultIcon ()
{
    CompIcon *i = screen->defaultIcon ();
    CompSize size;

    if (!i)
	return NULL;

    if (!i->width () || !i->height ())
	return NULL;

    if (priv->defaultIcon.icon == i)
	return priv->defaultIcon.textures[0];

    priv->defaultIcon.textures =
	GLTexture::imageBufferToTexture ((char *) i->data (), *i);

    if (priv->defaultIcon.textures.size () == 1)
	priv->defaultIcon.icon = i;
    else
    {
	priv->defaultIcon.icon = NULL;
	priv->defaultIcon.textures.clear ();
    }

    return priv->defaultIcon.textures[0];
}

void
GLScreen::resetRasterPos ()
{
    #ifndef USE_GLES
    glRasterPos2f (0, 0);
    #endif
    priv->rasterPos.setX (0);
    priv->rasterPos.setY (0);
}

